FROM registry.fedoraproject.org/fedora:rawhide
RUN dnf update -y && \
    dnf install -y \
        buildah \
        findutils \
        gcc \
        git \
        jq \
        make \
        podman \
        skopeo && \
    dnf clean all
